package com.neziaa.lobby.configuration;

import java.util.List;

import org.bukkit.Location;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.com.google.common.collect.Lists;

@Getter @Setter
public class Configuration {
	
	private List<Location> spawns = Lists.newArrayList();

}
