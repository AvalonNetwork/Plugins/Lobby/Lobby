package com.neziaa.lobby.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.neziaa.lobby.Lobby;
import com.neziaa.lobby.utils.DiscUtil;
import com.neziaa.lobby.utils.Saveable;

import net.minecraft.util.com.google.common.reflect.TypeToken;

public class ConfigurationManager implements Saveable {
	
	private Configuration configuration;
	
	@SuppressWarnings("serial")
	private TypeToken<Configuration> type = new TypeToken<Configuration>() {};
	
	public File getFile() {
		return new File(Lobby.getInstance().getDataFolder(), "configuration.json");
	}
	
	@Override
	public void load() {
		String content = DiscUtil.readCatch(this.getFile());
		
		if (content == null) return;
		
		try {
			
			this.configuration = Lobby.getInstance().getGson().fromJson(content, type.getType());
			
		} catch (Exception ex) {
			
			Bukkit.getLogger().log(Level.SEVERE, "Probl�me de configuration... " + ex);
			
		}
	}

	@Override
	public void save() {
		DiscUtil.writeCatch(getFile(), Lobby.getInstance().getGson().toJson(this.getConfiguration()), true);
	}
	
	public Configuration getConfiguration() {
		if (this.configuration == null)
			return this.configuration = new Configuration();
		return this.configuration;
	}

}
