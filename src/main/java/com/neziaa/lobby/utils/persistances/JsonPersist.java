package com.neziaa.lobby.utils.persistances;

import java.io.File;

import com.neziaa.lobby.Lobby;

import net.minecraft.util.com.google.gson.Gson;

public interface JsonPersist {
	
	public Gson gson = (Lobby.getInstance().getGson());
	
	public File getFile();
	
	public void loadData();
	
	public void saveData(boolean sync);
	

}
