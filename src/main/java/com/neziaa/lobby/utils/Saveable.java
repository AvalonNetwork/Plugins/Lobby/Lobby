package com.neziaa.lobby.utils;

public interface Saveable {
	
	/**
	 * Read.
	 */

	public void load(); 
	
	/**
	 * Write.
	 */
	
	public void save(); 
}
