package com.neziaa.lobby;

import java.lang.reflect.Modifier;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.google.common.collect.Lists;
import com.neziaa.lobby.configuration.ConfigurationManager;
import com.neziaa.lobby.utils.Saveable;
import com.neziaa.lobby.utils.persistances.adapter.ItemStackAdapter;
import com.neziaa.lobby.utils.persistances.adapter.LocationAdapter;
import com.neziaa.lobby.utils.persistances.adapter.PotionEffectAdapter;

import lombok.Getter;
import net.minecraft.server.v1_7_R4.ItemStack;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;

public class Lobby extends JavaPlugin {
	
	/**
	 * Instance
	 */
	
	@Getter private static Lobby instance;
	
	/**
	 * GSON Instance
	 */
	
	@Getter private Gson gson;
	
	/**
	 * Spawn(s)
	 */
	
	@Getter private List<Location> spawns;
	
	/**
	 * Saveables
	 */
	
	@Getter private List<Saveable> saveables = Lists.newArrayList();
	
	public void onEnable() {
		instance = this;
		
		/**
		 * GSON
		 */
		
		this.gson = this.getGsonBuilder().create();
		
		/**
		 * Instance ConfigurationManager
		 */
		
		ConfigurationManager configManager = new ConfigurationManager();
		
		/**
		 * Saveables
		 */
		
		saveables.add(configManager);
		saveables.forEach(s -> s.load());
		
		
		
	}
	
	private GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
        		.setPrettyPrinting()
        		.disableHtmlEscaping()
        		.serializeNulls()
        		.excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE)
        		.setDateFormat("dd/MM/yyyy HH:mm")
        		.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
        		.registerTypeAdapter(PotionEffect.class, new PotionEffectAdapter())
        		.registerTypeAdapter(Location.class, new LocationAdapter());
    }
	
	public void onDisable() {
		
	}

}
